package az.pogodemon.student.app.handler

import az.pogodemon.student.app.dto.ErrorResponse
import jakarta.servlet.http.HttpServletRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.time.LocalDateTime

@RestControllerAdvice
class GlobalExceptionHandler {
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleMethodArgumentNotValidException(
        exception: MethodArgumentNotValidException,
        request: HttpServletRequest,
    ): ResponseEntity<ErrorResponse> {
        val status: HttpStatus = HttpStatus.BAD_REQUEST
        return ResponseEntity.status(status)
            .body(
                ErrorResponse(
                    LocalDateTime.now(),
                    status.value().toShort(),
                    status.reasonPhrase,
                    exception.bindingResult.fieldErrors.joinToString(separator = ", ") { "${it.field} ${it.defaultMessage}" },
                    request.requestURI,
                ),
            )
    }
}
