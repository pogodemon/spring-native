package az.pogodemon.student.app.runtime

import az.pogodemon.student.app.entity.Student
import feign.Capability
import feign.ResponseInterceptor
import org.hibernate.generator.internal.CurrentTimestampGeneration
import org.hibernate.id.uuid.UuidGenerator
import org.springframework.aot.hint.ExecutableMode
import org.springframework.aot.hint.MemberCategory
import org.springframework.aot.hint.RuntimeHints
import org.springframework.aot.hint.RuntimeHintsRegistrar
import org.springframework.util.ReflectionUtils
import java.math.BigDecimal
import java.math.BigInteger
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.UUID

class StudentAppRuntimeHints : RuntimeHintsRegistrar {
    override fun registerHints(
        hints: RuntimeHints,
        classLoader: ClassLoader?,
    ) {
        hints.reflection()
            .registerType(UuidGenerator::class.java, MemberCategory.INVOKE_DECLARED_CONSTRUCTORS)
            .registerType(CurrentTimestampGeneration::class.java, MemberCategory.INVOKE_DECLARED_CONSTRUCTORS)
            .registerType(ResponseInterceptor.Chain::class.java)
            .registerMethod(
                ReflectionUtils.findMethod(Capability::class.java, "enrich", ResponseInterceptor.Chain::class.java)!!,
                ExecutableMode.INVOKE,
            )

        hints.serialization()
            .registerType(Student::class.java)
            .registerType(ArrayList::class.java)
            .registerType(Number::class.java)
            .registerType(BigDecimal::class.java)
            .registerType(BigInteger::class.java)
            .registerType(LocalDateTime::class.java)
            .registerType(LocalDate::class.java)
            .registerType(UUID::class.java)
    }
}
