package az.pogodemon.student.app.service.impl

import az.pogodemon.student.app.client.PublicApiEntriesClient
import az.pogodemon.student.app.dto.ApiEntriesResponse
import az.pogodemon.student.app.dto.KafkaMessage
import az.pogodemon.student.app.entity.Student
import az.pogodemon.student.app.kafka.MessageProducer
import az.pogodemon.student.app.repository.StudentRepository
import az.pogodemon.student.app.service.StudentService
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.stereotype.Service
import java.util.UUID
import java.util.concurrent.TimeUnit

@Service
class StudentServiceImpl(
    private val repository: StudentRepository,
    private val messageProducer: MessageProducer,
    private val redisTemplate: RedisTemplate<String, String>,
    private val publicApiEntriesClient: PublicApiEntriesClient,
) : StudentService {
    companion object {
        const val REDIS_PREFIX = "rt_"
    }

    @Cacheable("students")
    override fun getAllStudents(): List<Student> = repository.findAll()

    override fun getStudentById(id: UUID): Student = repository.findById(id).orElseThrow()

    override fun addNewStudent(student: Student) {
        repository.save(student)
    }

    override fun deleteStudentById(id: UUID) {
        repository.deleteById(id)
    }

    override fun publishKafkaMessage(message: KafkaMessage) {
        messageProducer.produceMessage(message)
    }

    override fun publishToRedisTemplate(
        key: String,
        value: String,
    ) {
        redisTemplate.opsForValue().set(REDIS_PREFIX + key, value, 20, TimeUnit.SECONDS)
    }

    override fun readFromRedisTemplate(key: String): String? = redisTemplate.opsForValue().get(REDIS_PREFIX + key)

    override fun getPublicApiEntries(): ApiEntriesResponse = publicApiEntriesClient.getAllPublicEntries()
}
