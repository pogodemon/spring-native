FROM redhat/ubi9
COPY build/native/nativeCompile/student-app-native /app
EXPOSE 8080 8081
CMD ["/app"]
