package az.pogodemon.student.app.aspect

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.aspectj.lang.annotation.Pointcut
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Aspect
@Component
class LoggingAspect {
    private val log: Logger = LoggerFactory.getLogger(LoggingAspect::class.java)

    @Pointcut("execution(* az.pogodemon.student.app.controller.*.*(..))")
    fun allControllerMethods() {
    }

    @Before("allControllerMethods()")
    fun logBeforeExecution(joinPoint: JoinPoint) {
        log.info("Before ${joinPoint.signature}")
    }

    @AfterReturning("allControllerMethods()")
    fun logAfterExecution(joinPoint: JoinPoint) {
        log.info("After ${joinPoint.signature}")
    }
}
