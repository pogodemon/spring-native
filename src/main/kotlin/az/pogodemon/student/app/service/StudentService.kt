package az.pogodemon.student.app.service

import az.pogodemon.student.app.dto.ApiEntriesResponse
import az.pogodemon.student.app.dto.KafkaMessage
import az.pogodemon.student.app.entity.Student
import java.util.UUID

interface StudentService {
    fun getAllStudents(): List<Student>

    fun getStudentById(id: UUID): Student

    fun addNewStudent(student: Student)

    fun deleteStudentById(id: UUID)

    fun publishKafkaMessage(message: KafkaMessage)

    fun publishToRedisTemplate(
        key: String,
        value: String,
    )

    fun readFromRedisTemplate(key: String): String?

    fun getPublicApiEntries(): ApiEntriesResponse
}
