package az.pogodemon.student.app.dto

data class ApiEntriesResponse(
    val count: Int?,
    val entries: List<ApiEntry?>?,
)
