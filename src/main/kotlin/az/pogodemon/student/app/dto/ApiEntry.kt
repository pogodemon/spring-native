package az.pogodemon.student.app.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class ApiEntry(
    @JsonProperty(value = "API")
    val api: String?,
    @JsonProperty(value = "Description")
    val description: String?,
    @JsonProperty(value = "Auth")
    val auth: String?,
    @JsonProperty(value = "HTTPS")
    val https: String?,
    @JsonProperty(value = "Cors")
    val cors: String?,
    @JsonProperty(value = "Link")
    val link: String?,
    @JsonProperty(value = "Category")
    val category: String?,
)
