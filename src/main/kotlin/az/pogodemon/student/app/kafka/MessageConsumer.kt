package az.pogodemon.student.app.kafka

import az.pogodemon.student.app.dto.KafkaMessage
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class MessageConsumer {
    private val log: Logger = LoggerFactory.getLogger(MessageProducer::class.java)

    @KafkaListener(topics = ["\${spring.kafka.testing-topic}"], groupId = "student-app-kafka-consumer")
    fun consumeMessage(message: KafkaMessage) {
        // used error log level for better visibility in console
        log.error("Logged kafka message: $message")
    }
}
