package az.pogodemon.student.app

import az.pogodemon.student.app.runtime.StudentAppRuntimeHints
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.ImportRuntimeHints

@ImportRuntimeHints(StudentAppRuntimeHints::class)
@SpringBootApplication
@EnableCaching
@EnableFeignClients
@OpenAPIDefinition(info = Info(title = "Student Application", description = "A description for the Student Application", version = "0.1"))
class StudentApp

fun main(vararg args: String) {
    SpringApplication.run(StudentApp::class.java, *args)
}
