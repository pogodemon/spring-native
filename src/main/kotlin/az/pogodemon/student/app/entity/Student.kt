package az.pogodemon.student.app.entity

import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table
import jakarta.validation.constraints.Past
import jakarta.validation.constraints.Pattern
import jakarta.validation.constraints.Positive
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.annotations.UuidGenerator
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.UUID

@Entity
@Table(name = "students")
data class Student(
    @field:Pattern(regexp = "^\\w+$")
    val firstName: String,
    @field:Pattern(regexp = "^\\w+$")
    val middleName: String?,
    @field:Pattern(regexp = "^\\w+$")
    val lastName: String,
    @field:Past
    val dateOfBirth: LocalDate?,
    @field:Pattern(regexp = "^\\w+$")
    val nationality: String?,
    @field:Pattern(regexp = "^\\w+$")
    var major: String,
    @field:Positive
    val admissionScore: BigDecimal,
) : Serializable {
    @Id
    @UuidGenerator
    val id: UUID? = null

    @CreationTimestamp
    val creationTimestamp: LocalDateTime = LocalDateTime.MIN

    @UpdateTimestamp
    val updateTimestamp: LocalDateTime? = null
}
