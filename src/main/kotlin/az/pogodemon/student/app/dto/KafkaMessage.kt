package az.pogodemon.student.app.dto

data class KafkaMessage(
    val data1: String?,
    val data2: String?,
)
