package az.pogodemon.student.app.client

import az.pogodemon.student.app.dto.ApiEntriesResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping

@FeignClient(name = "public-api-entries", url = "https://api.publicapis.org/entries")
interface PublicApiEntriesClient {
    @GetMapping
    fun getAllPublicEntries(): ApiEntriesResponse
}
