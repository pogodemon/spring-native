package az.pogodemon.student.app.kafka

import az.pogodemon.student.app.dto.KafkaMessage
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class MessageProducer(
    private val kafkaTemplate: KafkaTemplate<Any, KafkaMessage>,
) {
    @Value("\${spring.kafka.testing-topic}")
    private lateinit var topic: String

    fun produceMessage(message: KafkaMessage) {
        kafkaTemplate.send(topic, message)
    }
}
