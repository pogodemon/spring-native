package az.pogodemon.student.app.controller

import az.pogodemon.student.app.dto.ApiEntriesResponse
import az.pogodemon.student.app.dto.KafkaMessage
import az.pogodemon.student.app.entity.Student
import az.pogodemon.student.app.service.StudentService
import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("/api/v1/student")
@Tag(name = "Student Controller", description = "A description for the Student Controller")
class StudentController(
    private val service: StudentService,
    private val meterRegistry: MeterRegistry,
) {
    @GetMapping("/all")
    @Operation(summary = "Get all students")
    fun getAllStudents(): List<Student> {
        val counter: Counter = Counter.builder("requests_to_get_all_students").register(meterRegistry)
        counter.increment()
        return service.getAllStudents()
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get student by ID")
    fun getStudentById(
        @PathVariable id: UUID,
    ): Student {
        val counter: Counter = Counter.builder("requests_to_get_student_by_id").register(meterRegistry)
        counter.increment()
        return service.getStudentById(id)
    }

    @PostMapping
    @Operation(summary = "Add new student")
    fun addNewStudent(
        @RequestBody @Valid student: Student,
    ) {
        val counter: Counter = Counter.builder("requests_to_add_new_student").register(meterRegistry)
        counter.increment()
        service.addNewStudent(student)
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete student by ID")
    fun deleteStudentById(
        @PathVariable id: UUID,
    ) {
        val counter: Counter = Counter.builder("requests_to_delete_student_by_id").register(meterRegistry)
        counter.increment()
        service.deleteStudentById(id)
    }

    @PostMapping("/publish")
    @Operation(summary = "Publish Kafka message")
    fun publishKafkaMessage(
        @RequestBody message: KafkaMessage,
    ) {
        val counter: Counter = Counter.builder("requests_to_publish_kafka_message").register(meterRegistry)
        counter.increment()
        service.publishKafkaMessage(message)
    }

    @PostMapping("/redis-template")
    @Operation(summary = "Publish the Redis template")
    fun publishToRedisTemplate(
        @RequestParam key: String,
        @RequestParam value: String,
    ) {
        val counter: Counter = Counter.builder("requests_to_publish_to_redis_template").register(meterRegistry)
        counter.increment()
        service.publishToRedisTemplate(key, value)
    }

    @GetMapping("/redis-template")
    @Operation(summary = "Read from Redis template")
    fun readFromRedisTemplate(
        @RequestParam key: String,
    ): ResponseEntity<String> {
        val counter: Counter = Counter.builder("requests_to_read_from_redis_template").register(meterRegistry)
        counter.increment()
        return ResponseEntity.ok("""{"data":"${service.readFromRedisTemplate(key)}"}""")
    }

    @GetMapping("/public-api-entries")
    @Operation(summary = "Get public API entries")
    fun getPublicApiEntries(): ResponseEntity<ApiEntriesResponse> {
        val counter: Counter = Counter.builder("requests_to_get_public_api_entries").register(meterRegistry)
        counter.increment()
        return ResponseEntity.ok(service.getPublicApiEntries())
    }
}
