package az.pogodemon.student.app.config

import org.apache.kafka.clients.admin.NewTopic
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.TopicBuilder

@Configuration
class KafkaTopicConfig {
    @Bean
    fun createACompletelyUselessRandomKafkaTopic(): NewTopic {
        return TopicBuilder.name("az.pogodemon.student-app.kafka.completely.random.topic.just.for.testing")
            .partitions(13)
            .replicas(1)
            .build()
    }
}
